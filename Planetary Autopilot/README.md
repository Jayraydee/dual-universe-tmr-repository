#IDEA

This LUA will use the API to get all the planets.  
It will have a HUD element that will allow you to click and then pull up a list of all the planets in game.  
You can then click a planet and the ship will target that planet and accelerate to 30k and then brake via calculations of mass and braking power (it will also turn and burn for you)  
It will sound an alarm once you reach 1k speed to alert you to prep for re-entry.  
You will then have to click a HUD element to disable it to go back into manual mode otherwise (if you're afk?) it will brake until 0 to save you from ramming into the planet.  
It will stop you around 1su from the planet (or whatever is 0 gravity) so you do not plummet to the planet.  

Feel free to contribute and improve!  


#TODO
NEEDS:
I need somebody to help me with the formula in finding the distance/time to stop given the velocity and mass of a ship. The ones i found online do not convert things to meters per second. I need a detailed formual that I can program in.  
The plan is to query the game for mass and velocity and have the autopilot take that and calculate the stopping time. Then it will query the planet distance and initiate a brake/turn-and-burn when at that distance + 2su. 
